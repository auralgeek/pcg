from enum import Enum


class Ore(Enum):
    IRON = 1
    GOLD = 2
    COPPER = 3


class Size(Enum):
    SMALL = 1
    MEDIUM = 2
    LARGE = 3


class Mine():

    def __init__(self, size=Size.SMALL, ore=Ore.IRON):
        self.size = size
        self.ore = ore

    def set_size(self, size):
        self.size = size

    def __repr__(self):
        return "Size: {}\nOre: {}\n".format(self.size, self.ore)


if __name__ == '__main__':
    f = Mine()
    print(f)
