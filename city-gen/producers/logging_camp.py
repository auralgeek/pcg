from enum import Enum


class Size(Enum):
    SMALL = 1
    MEDIUM = 2
    LARGE = 3


class LoggingCamp():

    def __init__(self, size=Size.SMALL):
        self.size = size

    def set_size(self, size):
        self.size = size

    def __repr__(self):
        return "Size: {}\n".format(self.size)


if __name__ == '__main__':
    f = LoggingCamp()
    print(f)
