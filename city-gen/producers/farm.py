from enum import Enum


class Crop(Enum):
    WHEAT = 1
    BARLEY = 2
    POTATOES = 3
    CABBAGE = 4


class Size(Enum):
    SMALL = 1
    MEDIUM = 2
    LARGE = 3


class Farm():

    def __init__(self, size=Size.SMALL, crop=Crop.WHEAT):
        self.size = size
        self.crop = crop

    def set_size(self, size):
        self.size = size

    def __repr__(self):
        return "Size: {}\nCrop: {}\n".format(self.size, self.crop)


if __name__ == '__main__':
    f = Farm()
    print(f)
