import random
from house import Wealth, Size
from stall import Stall


class MarketDistrict():

    def __init__(self, n_stalls=10, wealth=Wealth.POOR):
        self.n_stalls = n_stalls
        self.wealth = wealth
        self.stalls = []
        self.__build()

    def __build(self):
        self.stalls = []
        for stall in range(self.n_stalls):
            new_stall = Stall()
            new_stall.wealth = self.__gen_stall_wealth()
            new_stall.size = self.__gen_stall_size()
            self.stalls.append(new_stall)

    def __gen_stall_wealth(self):
        if self.wealth == Wealth.POOR:
            dist = [0.9, 1.0, 1.0]
        elif self.wealth == Wealth.MIDDLE_CLASS:
            dist = [0.3, 0.8, 1.0]
        else:
            dist = [0.0, 0.4, 1.0]
        sam = random.random()
        if sam < dist[0]:
            return Wealth.POOR
        elif sam < dist[1]:
            return Wealth.MIDDLE_CLASS
        else:
            return Wealth.RICH

    def __gen_stall_size(self):
        if self.wealth == Wealth.POOR:
            dist = [0.8, 1.0, 1.0]
        elif self.wealth == Wealth.MIDDLE_CLASS:
            dist = [0.3, 0.8, 1.0]
        else:
            dist = [0.0, 0.4, 1.0]
        sam = random.random()
        if sam < dist[0]:
            return Size.SMALL
        elif sam < dist[1]:
            return Size.MEDIUM
        else:
            return Size.LARGE

    def get_stats(self):
        n_poor = len([h for h in self.stalls if h.wealth == Wealth.POOR])
        n_mid = len([h for h in self.stalls if h.wealth == Wealth.MIDDLE_CLASS])
        n_hi = len([h for h in self.stalls if h.wealth == Wealth.RICH])

        return (n_poor, n_mid, n_hi)

    def __repr__(self):
        n_poor, n_mid, n_hi = self.get_stats()
        return "# Stalls: {}\nWealth: {}\n# Poor: {}\n# Middle: {}\n# Rich: {}".format(self.n_stalls, self.wealth, n_poor, n_mid, n_hi)


if __name__ == '__main__':
    d1 = MarketDistrict(n_stalls=17, wealth=Wealth.MIDDLE_CLASS)
    print(d1)
