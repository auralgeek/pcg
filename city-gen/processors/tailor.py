from enum import Enum


class Wealth(Enum):
    POOR = 1
    MIDDLE_CLASS = 2
    RICH = 3


class Tailor():

    def __init__(self, wealth=Wealth.POOR):
        self.wealth = wealth

    def __repr__(self):
        return "Wealth: {}\n".format(self.wealth)


if __name__ == '__main__':
    f = Tailor()
    print(f)
