import random
from house import Wealth, Size, House

class District():

    def __init__(self, n_houses=10, wealth=Wealth.POOR):
        self.n_houses = n_houses
        self.wealth = wealth
        self.houses = []
        self.__build()

    def __build(self):
        self.houses = []
        for house in range(self.n_houses):
            new_house = House()
            new_house.set_wealth(self.__gen_house_wealth())
            new_house.set_size(self.__gen_house_size())
            self.houses.append(new_house)

    def __gen_house_wealth(self):
        if self.wealth == Wealth.POOR:
            dist = [0.9, 1.0, 1.0]
        elif self.wealth == Wealth.MIDDLE_CLASS:
            dist = [0.3, 0.8, 1.0]
        else:
            dist = [0.0, 0.4, 1.0]
        sam = random.random()
        if sam < dist[0]:
            return Wealth.POOR
        elif sam < dist[1]:
            return Wealth.MIDDLE_CLASS
        else:
            return Wealth.RICH

    def __gen_house_size(self):
        if self.wealth == Wealth.POOR:
            dist = [0.8, 1.0, 1.0]
        elif self.wealth == Wealth.MIDDLE_CLASS:
            dist = [0.3, 0.8, 1.0]
        else:
            dist = [0.0, 0.4, 1.0]
        sam = random.random()
        if sam < dist[0]:
            return Size.SMALL
        elif sam < dist[1]:
            return Size.MEDIUM
        else:
            return Size.LARGE

    def get_stats(self):
        n_poor = len([h for h in self.houses if h.wealth == Wealth.POOR])
        n_mid = len([h for h in self.houses if h.wealth == Wealth.MIDDLE_CLASS])
        n_hi = len([h for h in self.houses if h.wealth == Wealth.RICH])

        return (n_poor, n_mid, n_hi)

    def __repr__(self):
        n_poor, n_mid, n_hi = self.get_stats()
        return "# Houses: {}\nWealth: {}\n# Poor: {}\n# Middle: {}\n# Rich: {}".format(self.n_houses, self.wealth, n_poor, n_mid, n_hi)


if __name__ == '__main__':
    d1 = District(n_houses=17, wealth=Wealth.MIDDLE_CLASS)
    print(d1)
