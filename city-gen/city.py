import random
from house import Wealth
from district import District
from enum import Enum

class CitySize(Enum):

    VILLAGE = 1
    TOWN = 2
    CITY = 3
    CAPITAL = 4

class City():

    def __init__(self, size=CitySize.VILLAGE, wealth=Wealth.POOR):
        self.size = size
        self.wealth = wealth
        self.n_districts = 0
        self.districts = []
        self.__build()

    def __build(self):
        self.districts = []
        self.n_districts = self.__gen_number_of_districts()
        for d in range(self.n_districts):
            new_district = District(n_houses=8 + int(random.random() * 5), wealth=self.__gen_district_wealth())
            self.districts.append(new_district)

    def __gen_number_of_districts(self):
        if self.size == CitySize.VILLAGE:
            return 2 + int(random.random() * 3)
        elif self.size == CitySize.TOWN:
            return 5 + int(random.random() * 5)
        elif self.size == CitySize.CITY:
            return 10 + int(random.random() * 10)
        else:
            return 20 + int(random.random() * 10)

    def __gen_district_wealth(self):
        if self.wealth == Wealth.POOR:
            dist = [0.8, 0.9, 1.0]
        elif self.wealth == Wealth.MIDDLE_CLASS:
            dist = [0.4, 0.8, 1.0]
        else:
            dist = [0.3, 0.7, 1.0]
        sam = random.random()
        if sam < dist[0]:
            return Wealth.POOR
        elif sam < dist[1]:
            return Wealth.MIDDLE_CLASS
        else:
            return Wealth.RICH

    def get_stats(self):
        n_poor = 0
        n_mid = 0
        n_hi = 0
        for d in self.districts:
            d_poor, d_mid, d_hi = d.get_stats()
            n_poor += d_poor
            n_mid += d_mid
            n_hi += d_hi

        return (n_poor, n_mid, n_hi)

    def __repr__(self):
        n_poor, n_mid, n_hi = self.get_stats()
        return "# Districts: {}\nWealth: {}\n# Poor: {}\n# Middle: {}\n# Rich: {}".format(self.n_districts, self.wealth, n_poor, n_mid, n_hi)


if __name__ == '__main__':
    c1 = City(size=CitySize.CITY, wealth=Wealth.MIDDLE_CLASS)
    print(c1)
