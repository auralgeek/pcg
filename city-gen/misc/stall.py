from enum import Enum


class Size(Enum):
    SMALL = 1
    MEDIUM = 2
    LARGE = 3


class Wealth(Enum):
    POOR = 1
    MIDDLE_CLASS = 2
    RICH = 3


class Stall():

    def __init__(self):
        self.size = Size.SMALL
        self.wealth = Wealth.POOR

    def __repr__(self):
        return "Size: {}\nWealth: {}\n".format(self.size, self.wealth)


if __name__ == '__main__':
    s = Stall()
    print(s)
