import random
from enum import Enum


class Size(Enum):
    SMALL = 1
    MEDIUM = 2
    LARGE = 3

class Wealth(Enum):
    POOR = 1
    MIDDLE_CLASS = 2
    RICH = 3

class House():
    
    def __init__(self):
        self.size = Size.SMALL
        self.wealth = Wealth.POOR

    def set_size(self, size):
        self.size = size

    def set_wealth(self, wealth):
        self.wealth = wealth

    def __repr__(self):
        return "Size: {}\nWealth: {}\n".format(self.size, self.wealth)


def create_slum_district():
    slum = []
    done = False
    while not done:
        slum.append(House())
        if random.random() > 0.8:
            done = True
    return slum


if __name__ == '__main__':
    slum = create_slum_district()
    print(slum)
