from enum import Enum


class Size(Enum):
    SMALL = 1
    MEDIUM = 2
    LARGE = 3


class Wealth(Enum):
    POOR = 1
    MIDDLE_CLASS = 2
    RICH = 3


class Trader():

    def __init__(self, size=Size.SMALL, wealth=Wealth.POOR):
        self.size = size
        self.wealth = wealth

    def set_size(self, size):
        self.size = size

    def __repr__(self):
        return "Size: {}\nWealth: {}\n".format(self.size, self.wealth)


if __name__ == '__main__':
    f = Trader()
    print(f)
